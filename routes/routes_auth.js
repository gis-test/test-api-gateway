module.exports = {
  path: "/v1",
  //authorization: true,
   //  roles: ["ADMIN1"]
  aliases: {
    "GET user" : "users.get_docs",
    "POST user" : "users.insert_doc"
  },
  mappingPolicy: "restrict",
  bodyParsers: require('../config/default_bodyParsers.js'),
  
  onBeforeCall(ctx, route, req, res) {
    this.logger.info("onBeforeCall in protected route");
    console.log(req.body);
  },
  onAfterCall(ctx, route, req, res, data) {
    // Async function which return with Promise
    return data;
  },
  onError(req, res, err) {
    res.setHeader("Content-Type", "application/json");
    res.writeHead(err.code || 500);
    let resObj = {
      status: false,
      statusCode: err.code || 500,
      message: err.message,
      payload: err.data || {}
    }
    res.end(JSON.stringify(resObj));
  }
};
